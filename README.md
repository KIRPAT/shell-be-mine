# Shell-Be-Mine!

**Shell-Be-Mine!** is a portable shell environment setup tool that keeps your shell scripts and configurations in a single git source controlled folder. Think of it as a dot-files installation tool, but on steroids.

---
### HELP
- `sh shell-be-mine -h`


### INSTALL
- Clone this repository and start placing your stuff in the `src` folder.
- If you have **libraries** that you need to access from your shell, place them in the `./src/sbm-public-libs`.
- If you have **executable scripts** you need to access from your shell, place them in the `./src/sbm-public-scripts` 
- Open the `shell-be-mine` file with an editor and make changes on the default variables if you deem necessary.
    ```sh
    #VARIABLES
    SBM_SHELL="bash"
    SBM_SHELL_CONFIG_FILE_PATH="$HOME/.bashrc"
    ...
    ```
- Run the setup command.
  ```sh
  sh shell-be-mine -i
  ```
- **(Optional) Update with Git hooks** You can use git hooks to auto update your changes. `sh shell-be-mine -g` inserts the necessary command to `post-commit` git hook.

- **(Optional) Post-Install Script**  You might need to automate additional things after the setup. Such as, installing packages from a package manager. For that, create a script called `post-install-script` right next to the `shell-be-mine` script. Then run the following command: `sh shell-be-mine -s`. Your script will be run after the Shell-Be-Mine installation.
- To reference the read-only folder, use `$SBM_HOME` variable in your scripts.

### Uninstall

```sh
sh shell-be-mine -u
```

---

#### Under the hood...

-  The `shell-be-mine` script copies your shell scripts, libraries and config files to the **read-only** `~/.shell-be-mine/` folder. Then inserts a single line into your shell config file (ex:`~/.bashrc`). That line loads everything to your shell environment from the mentioned read-only folder. 


